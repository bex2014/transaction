package ru.bex.transaction.service;

import java.util.List;
import ru.bex.transaction.model.User;

public interface UserService {

	User save(final User newUser);

	List<User> findAll();

	User getOne(final Long userId);
}
