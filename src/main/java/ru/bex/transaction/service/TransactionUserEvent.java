package ru.bex.transaction.service;

import org.springframework.context.ApplicationEvent;
import ru.bex.transaction.model.User;

public class TransactionUserEvent extends ApplicationEvent {

	TransactionUserEvent(final User user) {
		super(user);
	}

	@Override
	public User getSource() {
		return (User) super.getSource();
	}
}
