package ru.bex.transaction.service;

import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class TransactionListener {

	@TransactionalEventListener
	public void afterCommitTransaction(final TransactionUserEvent event) {
		System.out.println("AFTER COMMIT: " + event.getSource().toString());
	}

	@TransactionalEventListener(phase = TransactionPhase.AFTER_ROLLBACK)
	public void afterRollbackTransaction(final TransactionUserEvent event) {
		System.out.println("AFTER ROLLBACK: " + event.getSource().toString());
	}
}
