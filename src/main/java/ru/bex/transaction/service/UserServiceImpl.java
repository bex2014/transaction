package ru.bex.transaction.service;

import java.util.List;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bex.transaction.model.User;
import ru.bex.transaction.repositories.UserRepo;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private final UserRepo userRepo;
	private final ApplicationEventPublisher publisher;

	public UserServiceImpl(final UserRepo userRepo, final ApplicationEventPublisher publisher) {
		this.userRepo = userRepo;
		this.publisher = publisher;
	}

	public User save(final User user) {
		final User savedUser = userRepo.save(user);
		publisher.publishEvent(new TransactionUserEvent(savedUser));
		return savedUser;
	}

	public List<User> findAll() {
		return userRepo.findAll();
	}

	public User getOne(final Long userId) {
		return userRepo.getOne(userId);
	}
}
