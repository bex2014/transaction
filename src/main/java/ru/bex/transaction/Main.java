package ru.bex.transaction;

import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.bex.transaction.config.Config;
import ru.bex.transaction.model.User;
import ru.bex.transaction.service.UserService;

public class Main {

	public static void main(String[] args) {
		final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class);
		final UserService userService = applicationContext.getBean(UserService.class);

		final User user1 = new User();
		user1.setId(1L);
		user1.setName("test1");
		user1.setEmail("test1");
		userService.save(user1);

		final User user2 = new User();
		user2.setId(2L);
		user2.setName("test2");
		user2.setEmail("test2");
		userService.save(user2);

		final List<User> userList = userService.findAll();
		System.out.println(userList);
	}

	static {
		org.apache.log4j.BasicConfigurator.configure();
	}
}
