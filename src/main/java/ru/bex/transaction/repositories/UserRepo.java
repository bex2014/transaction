package ru.bex.transaction.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bex.transaction.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

	List<User> findAll();

	User getOne(Long aLong);

	<S extends User> S save(S s);

	void delete(Long aLong);
}
