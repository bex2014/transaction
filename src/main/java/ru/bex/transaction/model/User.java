package ru.bex.transaction.model;

import static com.google.common.base.MoreObjects.toStringHelper;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "user", uniqueConstraints =
		{@UniqueConstraint(columnNames = {"id"}),
				@UniqueConstraint(columnNames = {"name"}),
				@UniqueConstraint(columnNames = {"email"})})
public class User {

	@Id
	@Column(nullable = false, unique = true)
	private Long id;
	@Column(nullable = false)
	private String email;
	@Column(nullable = false)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return toStringHelper(this)
				.add("id", id)
				.add("email", email)
				.add("name", name)
				.toString();
	}
}
